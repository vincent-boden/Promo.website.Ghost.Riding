﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Highscores_wegschrijven_ghost_riding
{
    public partial class HighscoreForm : Form
    {
        StreamReader inputStream;
        StreamWriter outputStream;
        private List<string> stringList = new List<string>();
        private List<int> scoreList = new List<int>();
        private int score = 6579;
        private string sourcepath;
        private string highscores;
        private string line;
        private string destination;
        private string nieuweHighscores;

        public HighscoreForm()
        {
            InitializeComponent();
            sourcepath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            highscores = Path.Combine(sourcepath, "C:/Users/" +
                    Environment.UserName + "/Desktop/Highscores Ghost Riding/Highscores.html");
            inputStream = File.OpenText(highscores);
            line = inputStream.ReadLine();
            inputStream.Close();
            destination = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            nieuweHighscores = Path.Combine(destination, "C:/Users/" +
                Environment.UserName + "/Desktop/Highscores Ghost Riding/Highscores.html");
            outputStream = File.CreateText(nieuweHighscores);
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            
            

            
            
            
                outputStream.WriteLine(line + "<div style='padding-top: 60px;'><p style='float: left; margin-left: 3%;'>{0} uit {1} heeft een score van:</p><p style='float: right; margin-right: 20%;'>{2}</p></div>",
                    textBox1.Text, textBox2.Text, score);
                this.Close();
           
            outputStream.Close();
            
        }
    }
}
